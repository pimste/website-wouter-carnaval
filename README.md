# Website wouter Carnaval

This is the code for the official website of "CV neet ketel mer kachel". It will be a platform where new members can sign up for the group and old members can review the page and pictures.


### Start

 - Clone the repository or download the files
 - Open in visual Studio Code or other editor
 - Open a virtual environment 
 - Run application using live server




### Installation

Download Bootstrap or follow the cdn in the code



### Project Status

There are still a lot of improvements that need to be done to make this project complete, but the beginning is there. If anyone wants to continue with the project, please follow the installation steps and improve the app!  
    
### Authors

- [@pimste](https://gitlab.com/pimste)
